package be.kdg.java2.demo;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello week 3");
        Student student = new Student("jos", 20, "Belg");
        MyFramework myFramework = new MyFramework();
        myFramework.inspectMethods(student);
        myFramework.runGetters("be.kdg.java2.demo.Student");
        try {
            myFramework.changeStudent(student);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        System.out.println("Changed?");
        System.out.println(student);
        myFramework.testHeavyMethods(student);
        new Testje().testje(student.getClass());
    }
}
