package be.kdg.java2.demo;

import be.kdg.java2.demo.annotations.Heavy;

import java.util.Objects;

public class Student {
    private String name;
    private int age;
    private String nationality;

    public Student() {
        this("dummy", 20, "Belg");
    }

    public Student(String name, int age, String nationality) {
        this.name = name;
        this.age = age;
        this.nationality = nationality;
    }

    @Heavy(100)
    public String getName() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Heavy(100)
    public String getNationality() {
        return nationality;
    }

    @Deprecated
    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return age == student.age && Objects.equals(name, student.name) && Objects.equals(nationality, student.nationality);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, age, nationality);
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", nationality='" + nationality + '\'' +
                '}';
    }
}
