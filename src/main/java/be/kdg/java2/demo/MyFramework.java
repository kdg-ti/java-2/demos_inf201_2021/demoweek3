package be.kdg.java2.demo;

import be.kdg.java2.demo.annotations.Heavy;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class MyFramework {
    public void inspectMethods(Object object) {
        Class clazz = object.getClass();
        System.out.println(clazz.getName());
        Method[] methods = clazz.getDeclaredMethods();
        int getterCount = 0;
        for (Method method : methods) {
            System.out.println(method.getName());
            if (method.getName().startsWith("get")) {
                getterCount++;
            }
            Class[] parameterTypes = method.getParameterTypes();
            if (parameterTypes.length > 0) {
                System.out.println("Has parameters:");
                for (Class parameterType : parameterTypes) {
                    System.out.println("\t" + parameterType.getName());
                }
            }
        }
        System.out.println("Found " + getterCount + " getters!");
    }

    public void runGetters(Object object) {
        Class clazz = object.getClass();
        Method[] methods = clazz.getDeclaredMethods();
        for (Method method : methods) {
            if (method.getName().startsWith("get")) {
                try {
                    System.out.println("Result of running " + method.getName() + ":");
                    System.out.println(method.invoke(object));
                } catch (IllegalAccessException | InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void runGetters(String className) {
        try {
            Class<?> clazz = Class.forName(className);
            Method[] methods = clazz.getDeclaredMethods();
            for (Method method : methods) {
                if (method.getName().startsWith("get")) {
                    Object object = clazz.getConstructor().newInstance();
                    System.out.println("Result of running " + method.getName() + ":");
                    System.out.println(method.invoke(object));
                }
            }
        } catch (ClassNotFoundException | InvocationTargetException | IllegalAccessException | InstantiationException | NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    public void changeStudent(Student student) throws NoSuchFieldException, IllegalAccessException {
        Class<? extends Student> studentClass = student.getClass();
        Field nameField = studentClass.getDeclaredField("name");
        nameField.setAccessible(true);
        nameField.set(student, "MARIE");
    }

    public void testHeavyMethods(Object object){
        System.out.println("Looking for heavy methods");
        Class<? extends Object> clazz = object.getClass();
        Method[] methods = clazz.getDeclaredMethods();
        for (Method method : methods) {
            Heavy heavy = method.getAnnotation(Heavy.class);
            if (heavy!=null) {
                System.out.println("Heavy method found:" + method.getName());
                int maxDuration = heavy.value();
                long beforeTime = System.currentTimeMillis();
                try {
                    method.invoke(object);
                    long deltaTime = System.currentTimeMillis() - beforeTime;
                    if (deltaTime>maxDuration) {
                        System.out.println("The method is too slow!");
                        System.out.println("It took " + deltaTime + " ms");
                        System.out.println("Max duration is: " + maxDuration);
                    }
                } catch (IllegalAccessException | InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
